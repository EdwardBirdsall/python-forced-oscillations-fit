import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq

# define function for power lorentzian
def power_lorenzian(x, Fm, omega0, gamma):
    numerator = x**2.0 * Fm**2.0
    denominator = (x**2.0 - omega0**2.0)**2.0 + gamma**2.0 * x**2.0
    return (numerator / denominator)

# define residuals function for nonlinear fit
def power_lorenzian_residuals(p, x, y, e):
    # extract parameters from parameter array
    Fm = p[0]
    omega0 = p[1]
    gamma = p[2]
    # calculate model
    model = power_lorenzian(x, Fm, omega0, gamma)
    # return residuals
    return ((y - model) / e)

# create x data for function (to plot)
x = np.linspace(14.0, 19.0, 1000) # min, max, number of points
# enter parameters
# these are initial guesses which are passed to the fitting algorithm
# their values will be changed after the fit is completed
# edit parameter values here
Fm = 7.0
omega0 = 17.0
gamma = 0.3

# load data from csv file
data = np.loadtxt(open("data.csv", "r"), delimiter=",")
# extract x, y, error data
x_data = data[:,0]
y_data = data[:,1] # colon = "all of"
e_data = data[:,2]

# create parameters array
p_init = [Fm, omega0, gamma]
# compute fit
p_out, p_cov, infodict, errmsg, success = leastsq(power_lorenzian_residuals, p_init, args=(x_data, y_data, e_data), full_output=1, epsfcn=1e-10)
# extract values from parameters array
Fm = p_out[0]
omega0 = p_out[1]
gamma = p_out[2]
# compute number of degrees of freedom
numberOfParameters = 3
degreesOfFreedom = len(x_data) - numberOfParameters
# create y data for function (to plot)
y = power_lorenzian(x, Fm, omega0, gamma)

# plot function
plt.plot(x, y, '-')
# plot data
plt.errorbar(x_data, y_data, yerr=e_data, fmt='_')

# set labels
plt.xlabel('Frequency [Hz]')
plt.ylabel('Power [mV^2]')

# draw and save, then display, figure
plt.draw()
plt.savefig('figure_2.png')
plt.show()

# compute parameter error values and chi-square value
if (len(y_data) > len(p_init)) and p_cov is not None:
    chi2 = (power_lorenzian_residuals(p_out, x_data, y_data, e_data)**2.0).sum() / (degreesOfFreedom)
    #chi2 = = (((y_data - power_lorenzian(x_data, Fm, omega0, gamma)) / e_data)**2.0).sum()
    p_cov = p_cov * chi2
else:
    p_cov = np.inf
p_err = []
for i in range(len(p_out)):
    try:
        next_v = np.absolute(p_cov[i][i])**0.5
        p_err.append(next_v)
    except:
        p_err.append(0.0)
p_err = np.array(p_err)
print ('chi2 = ', chi2)
print ('Fm = ', Fm, ' +- ', p_err[0])
print ('omega0 = ', omega0, ' +- ', p_err[1])
print ('gamma = ', gamma, ' +- ', p_err[2])